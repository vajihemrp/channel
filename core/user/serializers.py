from rest_framework import serializers
from .  import models

class AvatarSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.AvatarModel
        fields = '__all__'


class UserBizSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.UserBiz
        fields = '__all__'

    
class UserSerializerByJoin(serializers.ModelSerializer):
    avatar   = AvatarSerializer(read_only=True)
    user_biz = UserBizSerializer(read_only=True, many=True)

    class Meta:
        model = models.User
        fields = ('id', 'username', 'first_name', 'last_name', 'father', 'gender', 'avatar', 'user_biz')
