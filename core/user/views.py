from rest_framework.views import APIView
from rest_framework.response import Response
from user import serializers
from .models import User
from rest_framework import viewsets, status
from django.views.decorators.http import require_POST
import string
import secrets
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

class CheckToken(APIView):
    
    def get(self, request):
        
        # user
        user = request.user
        serializer = serializers.UserSerializerByJoin(user)
        json_data = serializer.data
        
        return Response({
            'detail'   : 'Token Is OK!!!',
            'user'      : json_data
        }) 

@csrf_exempt
@require_POST
def AccessUser(request):

    users = User.objects.all()
    serializer = serializers.UserSerializerByJoin(users, many=True)
    json_data = serializer.data
    # return Response("ok", status=status.HTTP_200_OK)
    # return JsonResponse({'message': 'room created'})
    # return Response({"user": list(users)}, status=status.HTTP_200_OK)

    return JsonResponse({'user': json_data})