from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
# from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _


class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()
    # password = models.CharField(_("password"))
    username = models.CharField(_("username"), max_length=15, unique=True)
    first_name = models.CharField(_("first name"), max_length=15)
    last_name = models.CharField(_("last name"), max_length=15)
    national_code = models.BigIntegerField(_("national code"))
    email = models.EmailField(_("email address"))
    mobile = models.CharField(_("Mobile number"), max_length=15)
    gender = models.CharField(choices=[('male','male'), ('female', 'female')], max_length=15)
    father = models.CharField(_("father name"), max_length=150, null=True)
    is_staff = models.BooleanField(_("staff status"))
    is_active = models.BooleanField(_("active"))
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = UserManager()
    
    EMAIL_FIELD = "email"
    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        # abstract = True

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class AvatarModel(models.Model):
    user    = models.OneToOneField(User(), on_delete=models.CASCADE, related_name='avatar', primary_key=True)
    image   = models.TextField()
    
    class Meta:
        db_table = 'user_avatar'
        # abstract = True
    
        
class UserBiz(models.Model):
    CHOICE_PERM = [
        ('owner','owner'),
        ('creator', 'creator'),
        ('viewer', 'viewer'),
        ('writer', 'writer'),
    ]
    
    user        = models.ForeignKey(User(), on_delete=models.CASCADE, related_name='user_biz')
    biz         = models.BigIntegerField()              # biz.biz.id
    permission  = models.CharField(max_length=7, choices=CHOICE_PERM)
    created_at  = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = 'user_biz'