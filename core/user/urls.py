from django.urls import path, include
from . import views

urlpatterns = [
    # path('usr/auth/', include())
    # path('user-review/', views.UserReviewView.as_view()),
    # path('login/', views.LoginView.as_view()),
    # path('register/', views.UserRegisterView.as_view()),
    # path('send-code/', views.SendCodeView.as_view()), # send sms code
    #
    # path('test-token/', views.TestToken.as_view()),

    path('', views.CheckToken.as_view()),
    path('all-users/', views.AccessUser, name='all-users'),
]