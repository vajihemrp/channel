from django.contrib import admin
from . models import User

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    ist_display = ['username', 'national_code', 'mobile', 'email', 'first_name', 'last_name']
