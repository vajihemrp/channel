import json
import time
from random import randint
from time import sleep

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async
from .models import Room, Message, JoinMember

class WSConsumer(WebsocketConsumer):

    def connect(self):
        print('hi!')
        self.accept()
        for i in range(1000):
            self.send(text_data=json.dumps({
                "message": randint(1, 100)
            }))
            sleep(1)
class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = f"chat_{self.room_name}"
        self.user_id = self.scope["url_route"]["kwargs"]["user_id"]

        print('connecting')

        # print(self.scope["client"])
        # print('connected')
        # print(self.scope["user"].id)

        # Join room group
        # await is used to call asynchronous functions that perform I/O.
        await self.get_room()

        user_exist = await self.check_room_member()
        if not user_exist:
            await self.close()

        connected_user = await self.channel_layer.group_add(self.room_group_name, self.channel_name)
        print('connected', connected_user)

        await self.accept()

        # connected_users = await self.channel_layer.group_channels(self.room_name)
        # print("Connected users:", connected_users)

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

        if not self.check_room_member:
            await self.set_room_closed()

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print(text_data + " is ok")
        message = text_data_json["message"]
        # type = text_data_json['type']
        user_name = text_data_json['user_name']
        user_id = int(text_data_json['user_id'])
        created_at=int(time.time())
        await self.create_message(user_name, message, user_id, created_at)

        # Send message to room group
        # chat.message calls the chat_message method
        await self.channel_layer.group_send(
            self.room_group_name, {
                "type": "chat_get_message",
                "message": message,
                'user_name': user_name,
                'user_id': user_id,
                'created_at': created_at,
            }
        )

    # Receive message from room group
    async def chat_get_message(self, event):
        message = event["message"]
        name = event["user_name"]
        agent = event['user_id']
        created_at = event['created_at']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({"message": message, 'user_name': name, "user_id": agent, 'created_at': created_at}))

    @sync_to_async
    def get_room(self):
        self.room = Room.objects.get(room_name=self.room_name)

    @sync_to_async
    def create_message(self, user_name, message, agent,created_at):
        message = Message.objects.create(body=message, user_name=user_name, user_id=agent, created_at=created_at)

        message.save()

        self.room.messages.add(message)

        return message

    @sync_to_async
    def check_room_member(self):
        join = JoinMember.objects.filter(room_id=self.room.id, member_id=self.user_id)

        return join.exists()

    @sync_to_async
    def set_room_closed(self):
        self.room = Room.objects.get(romm_name=self.room_name)
        self.room.status = Room.CLOSED
        self.room.save()
