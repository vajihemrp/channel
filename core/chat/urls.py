from django.urls import path
from django.conf.urls.static import static 
from django.conf import settings
from rest_framework import routers
from django.urls import path,include

# from .views import index
from . import views

# Create your views here.
urlpatterns = [
    path('test/', views.test_connection, name='test'),
    # path('', views.index, name='index'),
    # path("chat/<str:room_name>/", views.room, name="room"),

    #apis
    # path('create-room/', views.room_create, name='create-room'),
    # path('show-room/', views.show_room, name='room'),
    # path('user-rooms/', views.show_all_user_rooms, name='user-rooms'),
    # path('join-room/', views.join_room, name='join_room'),
    # path('attach/', views.attachFile, name='join_room'),

    # path('roomUser/', views.CheckToken2.as_view()),
    path('show-room/', views.ShowRoom.as_view()),
    path('create-public-room/', views.createPublicRoom.as_view()),
    path('add-member/', views.addMembersToRoom.as_view()),
    path('attach/', views.AttachFileApiView.as_view(), name='attach_file'),
    path('join-room/', views.JoinRoom.as_view(), name='join_room'),
    path('user-rooms/', views. showAllUserRooms.as_view(), name='user-rooms'),

    # path('chat-admin/', views.admin, name='admin'),
    # path('chat-admin/<str:uuid>/', views.admin_room, name='admin-room'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# router = routers.SimpleRouter()
# router.register('rooms/', views.RoomViewSet, basename='rooms')
# urlpatterns=[]
# urlpatterns += router.urls