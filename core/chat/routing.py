from django.urls import path
from django.urls import re_path

from . import consumers

ws_urlpatterns = [
    path('cht/ws/test-chat/', consumers.WSConsumer.as_asgi()),

    re_path(r"cht/ws/chat/(?P<room_name>\w+)/$",  consumers.ChatConsumer.as_asgi()),

]