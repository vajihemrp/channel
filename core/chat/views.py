import time
from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST
import string
import secrets
from django.views.decorators.csrf import csrf_exempt
from .models import Room, Message, Member, JoinMember, MessageFile
import json
from django.core import serializers
from django.http import Http404
from .serializer import (RoomSerializer, MessageSerializer, GetMessageSerializer, RoomPublicCreateSerializer,
                         RoomPublicCreateSerializer2, JoinRoomSerializer)
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.core.paginator import Paginator
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.views import APIView
from django.contrib.auth.decorators import login_required
from core.custom_lib import ApiResponse
# from rest_framework import Toke
from rest_framework.authentication import TokenAuthentication

# Create your views here.
class StringOperation:
    def generate_alphanumeric_string(length=16):
        alphabet = string.ascii_letters + string.digits
        token = ''.join(secrets.choice(alphabet) for _ in range(length))
        return token


def test_connection(request):
    return render(request, 'test.html', context={'text': 'welcome to chat'})

@csrf_exempt
@require_POST
def create_room(request):
    # room_name = request.POST.get('room_name', '')
    uuid = StringOperation.generate_alphanumeric_string(12)
    task_id = request.POST.get('task_id', None)
    members = request.POST.get('members', None)

    room = Room.objects.create(room_id=uuid, task_id=task_id, created_at= int(time.time()))

    objects_list = []

    for member in members:
        objects_list.append(Member(user_id=member, user_position=uuid, created_at=int(time.time())))

    # درج همزمان تمام اشیاء در دیتابیس
    members = Member.objects.bulk_create(objects_list)

    room.members.add(members)

    return JsonResponse({'message': 'room created'})

@csrf_exempt
@require_POST
def room_create(request):
    try:
        # تجزیه داده‌های JSON ارسالی
        data = json.loads(request.body)

        # اطمینان از اینکه داده‌های دریافتی یک لیست هستند
        if not isinstance(data['members'], list):
            raise ValueError('Expected a list of members')

        # انجام عملیات مورد نیاز با آرایه JSON
        uuid = StringOperation.generate_alphanumeric_string(12)
        task_id = data['task_id']
        members = data['members']
        create_by_id = data['user_id']
        room = Room.objects.create(room_name=uuid, task_id=task_id, create_by_id=create_by_id,
                                   created_at=int(time.time()))

        objects_list = []

        for member in members:
            # objects_list.append(Member(user_id=member['user_id'], user_position=member['user_position'], created_at=int(time.time())))
            memberRow = Member.objects.create(user_id=member['user_id'], user_position=member['user_position'],
                                              created_at=int(time.time()))
            room.members.add(memberRow)

        # درج همزمان تمام اشیاء در دیتابیس
        # members = Member.objects.bulk_create(objects_list)

        # members = Member.objects.create(body='ok', sent_by='vji')
        # room.members.add(members)
        # members.room.add(room)


        return JsonResponse({'message': 'Data received successfully!', 'data': data['task_id']})
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON'}, status=400)
    except ValueError as e:
        return JsonResponse({'error': str(e)}, status=400)

@csrf_exempt
@require_POST
def show_all_user_rooms(request):
    try:
        data = json.loads(request.body)

        user_id = data['user_id']

        member = Member.objects.filter(user_id=user_id)

        rooms_with_employees = Room.objects.filter(members__id=user_id,
                                                   members__joinmember__status__in=['joined', 'deactivate'])
        return JsonResponse({'message': 'Data received successfully!', 'data': list(rooms_with_employees.values())})

    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON'}, status=400)
    except ValueError as e:
        return JsonResponse({'error': str(e)}, status=400)


@csrf_exempt
@require_POST
def join_room(request):
    try:
        data = json.loads(request.body)

        room_id = int(data['room_id'])
        member_id = int(data['user_id'])

        # check user is member of room or not
        room = Room.objects.get(id=room_id)

        # try:
        join = JoinMember.objects.get(room_id=room_id, member_id=member_id)

        if join.status == JoinMember.DEACTIVATE:
            join.status = JoinMember.JOINED
            join.save()

        if room.status == Room.WAITING:
            room.status = Room.ACTIVE
            room.save()

        return JsonResponse({'message': 'Joined successfully!', 'data': []})

    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON'}, status=400)
    except ValueError as e:
        return JsonResponse({'error': str(e)}, status=400)


@csrf_exempt
# @require_POST
def attachFile(request):

    files = request.FILES.getlist('files')
    room_id = request.POST['room_id']
    user_id = request.user.id
    user_name = request.user.username
    massege_content = request.POST['message']
    return JsonResponse({'message': 'file attach successfully!', 'user_id': request.user.id, 'user_name':request.user.username })
    # add message body to the message
    room = Room.objects.get(id=room_id)

    massege = Message.objects.create(body=massege_content, user_name=user_name, user_id=user_id,
                                     created_at=int(time.time()))

    room.messages.add(massege)

    #  update room status
    if room.status == Room.WAITING:
        room.status = Room.ACTIVE
        room.save()

    # upload file
    uploaded_items = []
    for file in files:
        if file:
            type_file = file.content_type.split('/')
            if type_file[0] == 'image':
                uploaded_items.append(MessageFile(file_path=file, thumb_path=file, type=type_file[1], message=massege))
            else:
                uploaded_items.append(MessageFile(file_path=file, type=type_file[1], message=massege))

    # save  message file
    MessageFile.objects.bulk_create(uploaded_items)

    room_get_serializer = RoomSerializer(room)
    return JsonResponse({'message': 'file attach successfully!', 'data': room_get_serializer.data})


# @csrf_exempt
def show_room(request):
    # print(request.user.id)
    # if request.user.is_authenticated:
    #     logged_in_user = request.user
    #     user_info = {
    #         'username': logged_in_user.username,
    #         'email': logged_in_user.email,
    #         # دیگر اطلاعات مورد نیاز
    #     }
    #     print(request.user.id)
    # else:
    #     print('none') # کاربر لاگین نشده است
    # get all rooms messages without children
    room_id = request.GET.get('room_id')
    limit = request.GET.get('limit', 10)
    page_number = request.GET.get('page', 1)
    message_id = request.GET.get('message_id', 1)
    loc = request.GET.get('loc', 'gt')

    # return JsonResponse({'message': 'Joined successfully!', 'data': page_number})

    try:
        room = Room.objects.get(id=room_id)
    except Room.DoesNotExist:
        room = None

    if not room:
        response = {
            'count': 0,
            'next':  None,
            'previous': None,
            'results': [],
            'total-pages': 0
        }
        return JsonResponse(response)

    if loc == 'gt':
        message = room.messages.filter(id__gt=message_id)
    elif loc == 'gte':
        message = room.messages.filter(id__lt=message_id)
    elif loc == 'lte':
        message = room.messages.filter(id__lte=message_id)
    else:
        message = room.messages.filter(id__lt=message_id)

    paginator = Paginator(message, limit)  # Show 10 objects per page

    # Get the page number from the request
    page_obj = paginator.get_page(page_number)

    # Serialize the page objects
    serializer = MessageSerializer(page_obj, many= True)

    # Create a response dictionary with pagination details
    response = {
        'count': paginator.count,
        'next': page_obj.next_page_number() if page_obj.has_next() else None,
        'previous': page_obj.previous_page_number() if page_obj.has_previous() else None,
        'results': serializer.data,
        'total-pages': paginator.num_pages
    }

    # Return the paginated response as JSON
    return JsonResponse(response)

@csrf_exempt
def show_room2(request):
    # get all rooms messages with children
    room_id = request.GET.get('room_id')
    limit = request.GET.get('limit', 10)
    page_number = request.GET.get('page', 1)
    message_id = request.GET.get('message_id', 1)
    loc = request.GET.get('loc', 'gt')

    # return JsonResponse({'message': 'Joined successfully!', 'data': page_number})
    room = Room.objects.get(id=room_id)

    if loc == 'gt':
        message = room.messages.filter(id__gt=message_id)
    elif loc == 'gte':
        message = room.messages.filter(id__lt=message_id)
    elif loc == 'lte':
        message = room.messages.filter(id__lte=message_id)
    else:
        message = room.messages.filter(id__lt=message_id)

    paginator = Paginator(message, limit)  # Show 10 objects per page

    # Get the page number from the request
    page_obj = paginator.get_page(page_number)

    # Serialize the page objects
    serializer = MessageSerializer(page_obj, many=True)

    #  Create a response dictionary with pagination details
    response = {
        'count': paginator.count,
        'next': page_obj.next_page_number() if page_obj.has_next() else None,
        'previous': page_obj.previous_page_number() if page_obj.has_previous() else None,
        'results': serializer.data,
        'total-pages': paginator.num_pages
    }

    # Return the paginated response as JSON
    return JsonResponse(response)

class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    # @action()
    # def create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #
    #     # serializer.save()
    #
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['post'], url_path='task', url_name='task')
    def create_task(self, request, *args, **kwargs):
        return Response('ok', status=status.HTTP_201_CREATED)

# //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class CheckToken2(APIView):

    def get(self, request):
        # user
        user = request.user

        return Response({
            'detail': 'User Is OK!!!',
            'user': request.user.id
        }, status= status.HTTP_200_OK)

class ShowRoom(APIView):
    def get(self, request):
        room_id = request.GET.get('room_id')
        limit = request.GET.get('limit', 10)
        page_number = request.GET.get('page', 1)
        message_id = request.GET.get('message_id', 1)
        loc = request.GET.get('loc', 'gt')

        try:
            room = Room.objects.get(id=room_id)
        except Room.DoesNotExist:
            room = None

        if not room:
            data = {
                'count': 0,
                'next': None,
                'previous': None,
                'results': [],
                'total-pages': 0
            }
            return ApiResponse.success(self,"با موفقیت یافت شد", data)
            # return ApiResponse.error(self, "error", [], status.HTTP_404_NOT_FOUND)

        if loc == 'gt':
            message = room.messages.filter(id__gt=message_id)
        elif loc == 'gte':
            message = room.messages.filter(id__lt=message_id)
        elif loc == 'lte':
            message = room.messages.filter(id__lte=message_id)
        else:
            message = room.messages.filter(id__lt=message_id)

        paginator = Paginator(message, limit)  # Show 10 objects per page

        # Get the page number from the request
        page_obj = paginator.get_page(page_number)

        # Serialize the page objects
        serializer = MessageSerializer(page_obj, many=True)

        # Create a response dictionary with pagination details
        data = {
            'count': paginator.count,
            'next': page_obj.next_page_number() if page_obj.has_next() else None,
            'previous': page_obj.previous_page_number() if page_obj.has_previous() else None,
            'results': serializer.data,
            'total-pages': paginator.num_pages
        }

        # Return the paginated response as JSON
        return ApiResponse.success(self,"با موفقیت یافت شد", data)


class createTaskRoom(APIView):
    def post(self, request):
        try:
            # تجزیه داده‌های JSON ارسالی
            data = json.loads(request.body)

            # اطمینان از اینکه داده‌های دریافتی یک لیست هستند
            if not isinstance(data['members'], list):
                raise ValueError('Expected a list of members')

            # انجام عملیات مورد نیاز با آرایه JSON
            uuid = StringOperation.generate_alphanumeric_string(12)
            task_id = data['task_id']
            members = data['members']
            create_by_id = request.user.id
            room = Room.objects.create(room_name=uuid, task_id=task_id, create_by_id=create_by_id,
                                       created_at=int(time.time()))

            objects_list = []

            for member in members:
                # objects_list.append(Member(user_id=member['user_id'], user_position=member['user_position'], created_at=int(time.time())))
                memberRow = Member.objects.create(user_id=member['user_id'], user_position=member['user_position'],
                                                  created_at=int(time.time()))
                room.members.add(memberRow)

            # درج همزمان تمام اشیاء در دیتابیس
            # members = Member.objects.bulk_create(objects_list)

            # members = Member.objects.create(body='ok', sent_by='vji')
            # room.members.add(members)
            # members.room.add(room)

            # return JsonResponse({'message': 'Data received successfully!', 'data': data['task_id']})

            return ApiResponse.success(self,"با موفقیت ثبت شد", data)
        except json.JSONDecodeError:
            return JsonResponse({'error': 'Invalid JSON'}, status=400)
        except ValueError as e:
            return JsonResponse({'error': str(e)}, status=400)


class createPublicRoom(APIView):

    def post(self, request):
        try:
            create_by_id = request.user.id

            serializer = RoomPublicCreateSerializer2(data=request.data)
            if serializer.is_valid():
                # If data is valid, save it and return a success response by serializer create() function
                # serializer.save()

                roomObject = Room.objects.filter(room_name= request.data['room_name'], create_by_id=create_by_id)
                if roomObject.count():
                    return ApiResponse.error(self, "شما قبلا یک روم با همین نام ساخته اید" , [], status.HTTP_400_BAD_REQUEST)

                Room.objects.create(room_name=request.data['room_name'], create_by_id=create_by_id,
                                           created_at=int(time.time()))

                return ApiResponse.success(self, "با موفقیت ثبت شد", serializer.data)
            else:
                # If data is not valid, return an error response
                # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                return ApiResponse.error(self, "داده های ورودی نامعتبر هستند", serializer.errors, status.HTTP_400_BAD_REQUEST)

        except json.JSONDecodeError:
            return ApiResponse.error(self, "داده های ورودی نامعتبر هستند", [],
                                     status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return ApiResponse.error(self, str(e), [],
                                     status.HTTP_400_BAD_REQUEST)

class addMembersToRoom(APIView):
    def post(self,request):
        try:
            # تجزیه داده‌های JSON ارسالی
            data = json.loads(request.body)

            # اطمینان از اینکه داده‌های دریافتی یک لیست هستند
            if not isinstance(data['members'], list):
                return ApiResponse.error(self,"فیلدهای ورودی صحیح نیستند", [], status.HTTP_400_BAD_REQUEST)

            if not data['room_id']:
                return ApiResponse.error(self, "فیلدهای ورودی صحیح نیستند", [], status.HTTP_400_BAD_REQUEST)

            # انجام عملیات مورد نیاز با آرایه JSON
            room_id = data['room_id']
            members = data['members']

            try:
                room = Room.objects.get(id=room_id, create_by_id=request.user.id)
            except Room.DoesNotExist:
                return ApiResponse.error(self, "امکان افزودن کاربر برای شما وجود ندارد", [],
                                         status.HTTP_400_BAD_REQUEST)

            for member in members:
                # objects_list.append(Member(user_id=member['user_id'], user_position=member['user_position'], created_at=int(time.time())))
                memberRow = Member.objects.create(user_id=member['user_id'], user_position=member['user_position'],
                                                  created_at=int(time.time()))
                room.members.add(memberRow)

            # درج همزمان تمام اشیاء در دیتابیس
            # members = Member.objects.bulk_create(objects_list)

            # members = Member.objects.create(body='ok', sent_by='vji')
            # room.members.add(members)
            # members.room.add(room)

            return ApiResponse.success(self, "با موفقیت ثبت شد", [])
        except json.JSONDecodeError:
            return ApiResponse.error(self, "با موفقیت ثبت شد", [],status.HTTP_404_NOT_FOUND)
        except ValueError as e:
            return ApiResponse.error(self, str(e), [], status.HTTP_404_NOT_FOUND)

class AttachFileApiView(APIView):
    def post(self,request):
        files = request.FILES.getlist('files')
        room_id = request.POST['room_id']
        user_id = request.user.id
        user_name = request.user.username
        massege_content = request.POST['message']

        # add message body to the message
        room = Room.objects.get(id=room_id)

        massege = Message.objects.create(body=massege_content, user_name=user_name, user_id=user_id,
                                         created_at=int(time.time()))

        room.messages.add(massege)

        #  update room status
        if room.status == Room.WAITING:
            room.status = Room.ACTIVE
            room.save()

        # upload file
        uploaded_items = []
        for file in files:
            if file:
                type_file = file.content_type.split('/')
                if type_file[0] == 'image':
                    uploaded_items.append(
                        MessageFile(file_path=file, thumb_path=file, type=type_file[1], message=massege))
                else:
                    uploaded_items.append(MessageFile(file_path=file, type=type_file[1], message=massege))

        # save  message file
        MessageFile.objects.bulk_create(uploaded_items)

        room_get_serializer = RoomSerializer(room)
        return JsonResponse({'message': 'file attach successfully!', 'data': room_get_serializer.data})

class JoinRoom(APIView):
    def post(self,request):
        try:
            # data = json.loads(request.body)
            # room_id = int(data['room_id'])
            member_id = request.user.id

            serializer = JoinRoomSerializer(data=request.data)
            if serializer.is_valid():
                # check user is member of room or not

                room_id=serializer.data['room_id']

                room = Room.objects.get(id=room_id)

                # try:
                join = JoinMember.objects.get(room_id=room_id, member_id=member_id)

                if join.status == JoinMember.DEACTIVATE:
                    join.status = JoinMember.JOINED
                    join.save()

                if room.status == Room.WAITING:
                    room.status = Room.ACTIVE
                    room.save()

                return ApiResponse.success(self, "با موفقیت عضو شدید", [])

            else:
                # If data is not valid, return an error response
                # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                return ApiResponse.error(self, "داده های ورودی نامعتبر هستند", serializer.errors,
                                         status.HTTP_400_BAD_REQUEST)

        except json.JSONDecodeError:
            return ApiResponse.error(self, "ورودی نامعتبر است", [], status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return ApiResponse.error(self, str(e), [], status.HTTP_404_NOT_FOUND)

class showAllUserRooms(APIView):

    def post(self,request):
        try:
            # data = json.loads(request.body)

            user_id = request.user.id

            # member = Member.objects.filter(user_id=user_id)

            rooms_with_employees = Room.objects.filter(members__id=user_id,
                                                       members__joinmember__status__in=['joined', 'deactivate'])
            # return JsonResponse({'message': 'Data received successfully!', 'data': list(rooms_with_employees.values())})

            return ApiResponse.success(self, "با موفقیت یافت شد", list(rooms_with_employees.values()))
        except json.JSONDecodeError:
            return ApiResponse.error(self, "درخواست نامعتبر است", [], status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            # return JsonResponse({'error': str(e)}, status=400)
            return ApiResponse.error(self, str(e), [], status.HTTP_400_BAD_REQUEST)