from django.contrib.auth.models import User
from django.db import models
from django_resized import ResizedImageField


# Create your models here.
class Message(models.Model):
    body = models.TextField()
    user_name = models.CharField(max_length=255, null=True, blank=True)
    user_id = models.PositiveBigIntegerField(null=True)
    created_at = models.PositiveBigIntegerField(null=True)
    updated_at = models.PositiveBigIntegerField(null=True)
    deleted_at = models.PositiveBigIntegerField(null=True)
    parent = models.ForeignKey('self', on_delete=models.DO_NOTHING, null=True, blank=True, related_name='reply_of')

    class Meta:
        ordering = ('created_at',)

    def __str__(self):
        return f'{self.user_name}'

class Member(models.Model):
    user_id = models.PositiveBigIntegerField()
    user_name = models.CharField(max_length=255, null=True)
    user_position = models.CharField(max_length=255)
    role = models.CharField(max_length=255, null=True)
    created_at = models.PositiveBigIntegerField(null=True)
    updated_at = models.PositiveBigIntegerField(null=True)
    deleted_at = models.PositiveBigIntegerField(null=True)

    def __str__(self):
        return f'{self.user_name} - {self.user_position}'

class Room(models.Model):
    WAITING = 'waiting'
    ACTIVE = 'active'
    CLOSED = 'closed'

    CHOICES_STATUS = (
        (WAITING, 'Waiting'),
        (ACTIVE, 'Active'),
        (CLOSED, 'Closed'),
    )

    room_name = models.CharField(max_length=255, null=True)
    task_id = models.PositiveBigIntegerField(null=True)
    messages = models.ManyToManyField(Message, blank=True)
    members = models.ManyToManyField(Member, through="JoinMember",blank=True)
    create_by_id = models.PositiveBigIntegerField(null=True)
    status = models.CharField(max_length=20, choices=CHOICES_STATUS, default=WAITING)
    created_at = models.PositiveBigIntegerField(null=True)
    updated_at = models.PositiveBigIntegerField(null=True)
    deleted_at = models.PositiveBigIntegerField(null=True)

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return f'{self.room_name}'

class JoinMember(models.Model):
    JOINED = 'joined'
    REMOVED = 'removed'
    DEACTIVATE = 'deactivate'

    CHOICES_STATUS = (
        (JOINED, 'joined'),
        (REMOVED, 'removed'),
        (DEACTIVATE, 'deactivate'),
    )

    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=CHOICES_STATUS, default=DEACTIVATE)
    created_at = models.PositiveBigIntegerField(null=True)
    updated_at = models.PositiveBigIntegerField(null=True)
    deleted_at = models.PositiveBigIntegerField(null=True)
    class Meta:
        ordering = ('-created_at',)
        unique_together = (('room', 'member'),)
        db_table = 'chat_room_member'

class MessageFile(models.Model):
    file_path = models.FileField(upload_to='attachments/', blank=True, null=True)
    thumb_path = ResizedImageField(size=[640, 480], upload_to='attachments/thumb/', null=True)
    type = models.CharField(max_length=50, null=True)
    message = models.ForeignKey('Message', on_delete=models.CASCADE, null=True, related_name='files')
    created_at = models.PositiveBigIntegerField(null=True)
    updated_at = models.PositiveBigIntegerField(null=True)
    deleted_at = models.PositiveBigIntegerField(null=True)

    class Meta:
        ordering = ('-created_at',)
        db_table = "chat_message_files"

    def __str__(self):
        return f'{self.type}'


