from .models import MessageFile, Message, Room, Member
from rest_framework import serializers
from django.conf import settings


class FileUrlField(serializers.Field):
    def to_representation(self, value):
        if not value:
            return None
        return f"{settings.BACKEND_URL}{value}"


class FileSerializer(serializers.ModelSerializer):
    full_file_url = FileUrlField(source='file_path')
    full_thumb_url = FileUrlField(source='thumb_path')

    class Meta:
        model = MessageFile
        fields = ['file_path', 'thumb_path', 'type', 'full_file_url', 'full_thumb_url']

class RecursiveSerializer(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data

class ParentSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, allow_null=True, required=False)
    class Meta:
        model =  Message
        fields = ['body','user_name','user_id', 'files', 'created_at', 'updated_at']

class MessageSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, allow_null=True, required=False)
    # reply_of = RecursiveSerializer(many=True) # get parent
    # reply_of = serializers.SerializerMethodField() #get parent

    # get children
    reply_of = ParentSerializer(source='parent', read_only=True)

    class Meta:
        model = Message
        fields = ['body','user_name','user_id', 'files', 'created_at', 'updated_at','reply_of']


    # when using reply_of = serializers.SerializerMethodField() to get parent
    # def get_reply_of(self, obj):
    #     # Assuming 'children' is the related name for the child relationship
    #     return MessageSerializer(obj.reply_of.all(), many=True).data

class GetMessageSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True, allow_null=True, required=False)

    class Meta:
        model = Message
        fields = ['body','user_name','user_id', 'files', 'created_at', 'updated_at']

class MemberSerialozer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = '__all__'
        # fields = ['room_name','task_id','create_by_id','status']


class RoomSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True, allow_null=True, required=False)
    members = MemberSerialozer(many=True, allow_null=True, required=False)

    class Meta:
        model = Room
        # fields = '__all__'
        fields = ['room_name','task_id','create_by_id','status','messages', 'members']


class RoomPublicCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        # fields = '__all__'
        fields = ['room_name','create_by_id','status']

        def validate(self, data):
            # Perform your custom validation here
            if 'room_name' in data:
                raise serializers.ValidationError({'room_name': 'Invalid value'})
            return data


class RoomPublicCreateSerializer2(serializers.Serializer):
    # Explicitly define each field
    room_name = serializers.CharField(max_length=200)
    # another_field = serializers.IntegerField()

    # def create(self, validated_data):
    #     # Implement the creation logic
    #     return Room.objects.create(**validated_data)
    #
    # def update(self, instance, validated_data):
    #     # Implement the update logic
    #     instance.my_field = validated_data.get('room_name', instance.my_field)
    #     # instance.another_field = validated_data.get('another_field', instance.another_field)
    #     instance.save()
    #     return instance

class JoinRoomSerializer(serializers.Serializer):
    # Explicitly define each field
    room_id = serializers.IntegerField()