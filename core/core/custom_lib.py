from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings


# def cbool(value):
#     if isinstance(value, str):
#         value = value.lower()
#
#     if value in ['true', 'yes', '1']:
#         return True
#
#     return False
#
#
# class PageNumberPagination(PageNumberPagination):
#     """Page Number Pagination, added total pages to output
#         add to setting.py
#             settings.REST_FRAMEWORK['PAGE_SIZE']
#             settings.REST_FRAMEWORK['PAGE_SIZE_QUERY_PARAM']
#             settings.REST_FRAMEWORK['MAX_PAGE_SIZE']
#             settings.REST_FRAMEWORK['TOTAL_PAGES_QUERY_PARAM']
#     """
#     page_size = settings.REST_FRAMEWORK['PAGE_SIZE']
#     page_size_query_param = settings.REST_FRAMEWORK['PAGE_SIZE_QUERY_PARAM']
#     max_page_size = settings.REST_FRAMEWORK['MAX_PAGE_SIZE']
#
#     def get_paginated_response(self, data):
#         response = super().get_paginated_response(data)
#         response.data[settings.REST_FRAMEWORK['TOTAL_PAGES_QUERY_PARAM']] = self.page.paginator.num_pages
#         return response
#
#         ''' FOR USE FROM CODES
#
#         from config.custom_lib import PageNumberPagination,
#
#         class ClassName(viewsets.ModelViewSet):
#             .
#             .
#             pagination_class = PageNumberPagination
#             1403/02/01 taneh
#         '''
#
#
# class QueryParams:
#
#     def get_page_size(self, request):
#         """Get page_size from the client, If it was not sent, it will use the default from setting.py
#             add to setting.py
#                 settings.REST_FRAMEWORK['PAGE_SIZE']
#                 settings.REST_FRAMEWORK['PAGE_SIZE_QUERY_PARAM']
#         """
#         page_number = request.query_params.get(settings.REST_FRAMEWORK['PAGE_SIZE_QUERY_PARAM'])
#
#         if page_number and page_number.isdigit():
#             return int(page_number)
#         else:
#             return settings.REST_FRAMEWORK['PAGE_SIZE']
#
#         ''' FOR USE FROM CODES
#
#             from config.custom_lib import Queryset
#
#             class ClassName(viewsets.ModelViewSet):
#                 .
#                 .
#
#             def get_queryset(self):
#                 _queryset = Queryset()
#                 self.pagination_class.page_size = _queryset.get_page_size(self.request)
#                 return super().get_queryset()
#                 1403/02/01 taneh
#         '''
#
#     def check_perfect(self, request):
#         """If sent perfect=true, returns all fields with joins to tables"""
#         return cbool(request.query_params.get('perfect', False))
#
#     def get_search_fields(self, request, default_search_fields=[]):
#         """If there is a field or fields entered for search, it uses them,
#             otherwise it uses the default value that is in the codes.
#
#             send: url.com?search_fields=field1,field2,...
#         """
#         search_fields = request.query_params.get('search_fields')
#         if search_fields:
#
#             if ',' in search_fields:
#                 return search_fields.split(',')
#             else:
#                 return [search_fields]
#
#         else:
#             return default_search_fields
#
#         ''' FOR USE FROM CODES
#
#             from rest_framework.pagination import PageNumberPagination
#
#             class ClassName(viewsets.ModelViewSet):
#                 .
#                 .
#                 pagination_class = PageNumberPagination
#                 filter_backends = [SearchFilter]
#
#             def list(self, request, *args, **kwargs):
#                 SearchFields = SearchFields()
#                 self.search_fields = SearchFields.get_search_fields(request, ['parent'])
#                 return super().list(request, *args, **kwargs)
#                 1403/02/01 taneh
#         '''
#
#
# class Language:
#     """get language from the client, If it was not sent, it will use the default en.
#
#         return: string
#     """
#
#     def get_lang(self, request, default_lang='en'):
#         lang = request.META.get('HTTP_ACCEPT_LANGUAGE', default_lang)
#         return lang.split('-'[0])[0]
#
#         '''
#         if Language().get_lang(request) == 'fa':
#                 message = 'رکوردهایی با مقادیر فیلد یکسان از قبل وجود دارد!'
#             else:
#                 message = 'Records with the same field values already exist.'
#             1403/02/05 taneh
#         '''
#
#
# class Error():
#     def error_method(self, request):
#         if Language().get_lang(request) == 'fa':
#             message = 'متد مجاز نیست.'
#         else:
#             message = 'method is not allowed.'
#         return Response({"error": message}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

class ApiResponse:

    def success(self, message, data):
        response = {
            'success': True,
            'data':  data,
            'message': message,
        }
        return Response(response, status=status.HTTP_200_OK)


    def error(self, message, errorMessages, s):
        response = {
            'success': False,
            'message': message,
            'errors': errorMessages
        }

        # if errorMessages:
        #     response.errors= errorMessages

        return Response(response,status=s)


